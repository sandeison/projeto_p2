TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    avl.c \
    cp.c \
    list.c

HEADERS += \
    cp.h

DISTFILES += \
    script.r

